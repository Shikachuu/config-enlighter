FROM composer:2 as dependecnyInstaller
COPY backend /app
WORKDIR /app
RUN composer i -o --no-dev --ignore-platform-reqs --prefer-dist --no-progress --no-scripts

FROM php:8.0-apache
COPY ./infrastructure/apache2.conf /etc/apache2/apache2.conf
COPY ./infrastructure/custom.php.ini /usr/local/etc/php/conf.d/custom.ini
COPY ./infrastructure/vhost.conf /etc/apache2/sites-available/000-default.conf
COPY --from=dependecnyInstaller /app /var/www
RUN apt update \
    && apt install libonig-dev \
    && docker-php-ext-install mbstring opcache \
    && apt -y autoremove \
    && apt clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && a2enmod rewrite \
    && chown -R $APACHE_RUN_GROUP:$APACHE_RUN_USER /var/www
USER $APACHE_RUN_GROUP:$APACHE_RUN_USER