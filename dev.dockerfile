FROM ubuntu:20.04

LABEL maintainer="Shikachuu"

WORKDIR /app

ENV DEBIAN_FRONTEND noninteractive
ENV TZ=CET

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update \
    && apt-get install -y gnupg curl ca-certificates zip unzip git sqlite3 \
    && mkdir -p ~/.gnupg \
    && chmod 600 ~/.gnupg \
    && echo "disable-ipv6" >> ~/.gnupg/dirmngr.conf \
    && apt-key adv --homedir ~/.gnupg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys E5267A6C \
    && apt-key adv --homedir ~/.gnupg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C300EE8C \
    && echo "deb http://ppa.launchpad.net/ondrej/php/ubuntu focal main" > /etc/apt/sources.list.d/ppa_ondrej_php.list \
    && apt update \
    && apt install -y php8.0-cli php8.0-dev php8.0-sqlite3 php8.0-curl php8.0-mbstring php8.0-zip \
    && php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer \
    && apt -y autoremove \
    && apt clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    "enlighter"

COPY infrastructure/custom.php.ini /etc/php/8.0/cli/conf.d/99-enlighter.ini

EXPOSE 8080

USER enlighter:enlighter

CMD ["/usr/bin/php", "-d", "variables_order=EGPCS", "-S", "0.0.0.0:8080", "-t", "/app/public/"]