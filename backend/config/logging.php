<?php

declare(strict_types=1);

use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\StreamHandler;

return [
    'default' => env('LOG_CHANNEL', 'stdout'),
    'channels' => [
        'testing' => [
            'driver' => 'null'
        ],
        'stdout' => [
            'driver' => 'monolog',
            'handler' => StreamHandler::class,
            'formatter' => JsonFormatter::class,
            'level' => 'info',
            'with' => [
                'stream' => 'php://stdout',
            ],
            'channels' => ['production', 'local'],
        ]
    ]
];
