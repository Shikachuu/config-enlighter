<?php

namespace Database\Seeders;

use App\Models\Configuration;
use Database\Factories\ConfigurationFactory;
use Faker\Factory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

class ConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var Collection $configurations */
        $configurations = Configuration::factory(30)->create();
        $configurations->each(function (Configuration $configuration) {
            $configuration->addRevision(Factory::create()->realTextBetween(50));
        });
    }
}
