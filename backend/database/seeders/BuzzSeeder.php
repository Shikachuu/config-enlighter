<?php

namespace Database\Seeders;

use App\Models\Word;
use Illuminate\Database\Seeder;

class BuzzSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Word::create([
            'name' => 'ar',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'vr',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'paas',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'saas',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'cloud',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'container',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'kubernetes',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'quantumcomputing',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'machinelearning',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'artificialinteligence',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'iot',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'fiveg',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'edge',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'edgecloud',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'blockchain',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'voicerecognition',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'robotics',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'bitcoin',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'microservices',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'rdbsm',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'vpn',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'innovation',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'fullstack',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'homeoffice',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'intranet',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'metadata',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'digitalsignature',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'devops',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'virtualmachine',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'docker',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'kernel',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'highavailability',
            'type' => 'buzz'
        ]);
        Word::create([
            'name' => 'framework',
            'type' => 'buzz'
        ]);
    }
}
