<?php

namespace Database\Seeders;

use App\Models\Word;
use Illuminate\Database\Seeder;

class NounSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Word::create([
            'name' => 'account',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'act',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'adjustment',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'advertisement',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'agreement',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'air',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'amount',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'amusement',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'animal',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'answer',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'apparatus',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'approval',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'argument',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'art',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'attack',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'attempt',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'attention',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'attraction',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'authority',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'back',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'balance',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'base',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'behavior',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'belief',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'birth',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'bit',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'bite',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'blood',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'blow',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'body',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'brass',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'bread',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'breath',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'brother',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'building',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'burn',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'burst',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'business',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'butter',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'canvas',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'care',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'cause',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'chalk',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'chance',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'change',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'cloth',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'coal',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'color',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'comfort',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'committee',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'company',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'comparison',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'competition',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'condition',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'connection',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'control',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'cook',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'copper',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'copy',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'cork',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'copy',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'cough',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'country',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'cover',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'crack',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'credit',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'crime',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'crush',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'cry',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'current',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'curve',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'damage',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'danger',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'daughter',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'day',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'death',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'debt',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'decision',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'degree',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'design',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'desire',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'destruction',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'detail',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'development',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'digestion',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'direction',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'discovery',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'discussion',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'disease',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'disgust',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'distance',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'distribution',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'division',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'doubt',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'drink',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'driving',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'dust',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'earth',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'edge',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'education',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'effect',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'end',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'error',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'event',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'example',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'exchange',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'existence',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'expansion',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'experience',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'expert',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'fact',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'fall',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'family',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'father',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'fear',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'feeling',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'fiction',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'field',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'fight',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'fire',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'flame',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'flight',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'flower',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'fold',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'food',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'force',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'form',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'friend',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'front',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'fruit',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'glass',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'gold',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'government',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'grain',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'grass',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'grip',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'group',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'growth',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'guide',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'harbor',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'harmony',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'hate',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'hearing',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'heat',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'help',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'history',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'hole',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'hope',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'hour',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'humor',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'ice',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'idea',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'impulse',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'increase',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'industry',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'ink',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'insect',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'instrument',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'insurance',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'interest',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'invention',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'iron',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'jelly',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'join',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'journey',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'judge',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'jump',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'kick',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'kiss',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'knowledge',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'land',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'language',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'laugh',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'low',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'lead',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'learning',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'leather',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'letter',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'level',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'lift',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'light',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'limit',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'linen',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'liquid',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'list',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'look',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'loss',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'love',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'machine',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'man',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'manager',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'mark',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'market',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'mass',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'meal',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'measure',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'meat',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'meeting',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'memory',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'metal',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'middle',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'milk',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'mind',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'mine',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'minute',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'mist',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'money',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'month',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'morning',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'mother',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'motion',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'mountain',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'move',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'music',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'name',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'nation',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'need',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'news',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'night',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'noise',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'note',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'number',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'observation',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'offer',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'oil',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'operation',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'opinion',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'order',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'organization',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'ornament',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'owner',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'page',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'pain',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'paint',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'paper',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'part',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'paste',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'payment',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'peace',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'person',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'place',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'plant',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'play',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'pleasure',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'point',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'poison',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'polish',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'porter',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'position',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'powder',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'power',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'price',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'print',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'process',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'produce',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'profit',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'property',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'prose',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'protest',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'pull',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'punishment',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'purpose',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'push',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'quality',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'question',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'rain',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'range',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'rate',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'ray',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'reaction',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'reading',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'reason',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'record',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'regret',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'relation',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'religion',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'representative',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'request',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'respect',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'rest',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'reward',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'rhythm',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'rice',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'river',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'road',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'roll',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'room',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'rub',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'rule',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'run',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'salt',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'sand',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'scale',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'science',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'sea',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'seat',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'secretary',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'selection',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'self',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'sense',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'servant',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'sex',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'shade',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'shake',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'shame',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'shock',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'side',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'sign',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'silk',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'silver',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'sister',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'size',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'sky',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'sleep',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'slip',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'slope',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'smash',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'smell',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'smile',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'smoke',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'sneeze',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'snow',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'soap',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'society',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'son',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'song',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'sort',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'sound',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'soup',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'space',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'stage',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'start',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'statement',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'steam',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'steel',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'step',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'stitch',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'stone',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'stop',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'story',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'stretch',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'structure',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'substance',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'sugar',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'suggestion',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'summer',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'support',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'surprise',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'swim',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'system',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'talk',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'taste',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'tax',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'teaching',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'tendency',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'test',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'theory',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'thing',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'thought',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'thunder',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'time',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'tin',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'top',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'touch',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'trade',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'transport',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'trick',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'trouble',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'turn',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'twist',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'unit',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'use',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'value',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'verse',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'vessel',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'view',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'voice',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'walk',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'war',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'wash',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'waste',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'water',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'wave',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'wax',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'way',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'weather',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'week',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'weight',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'wind',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'wine',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'winter',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'woman',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'wood',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'wool',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'word',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'work',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'wound',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'writing',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'year',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'angle',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'ant',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'apple',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'arch',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'arm',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'army',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'baby',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'bag',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'ball',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'band',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'basin',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'basket',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'bath',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'bed',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'bee',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'bell',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'berry',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'bird',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'blade',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'board',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'boat',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'bone',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'book',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'boot',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'bottle',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'box',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'boy',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'brain',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'brake',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'branch',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'brick',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'bridge',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'brush',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'bucket',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'bulb',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'button',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'cake',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'camera',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'card',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'carriage',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'cart',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'cat',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'chain',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'cheese',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'chess',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'chin',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'church',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'circle',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'clock',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'cloud',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'coat',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'collar',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'comb',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'cord',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'cow',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'cup',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'curtain',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'cushion',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'dog',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'door',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'drain',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'drawer',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'dress',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'drop',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'ear',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'egg',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'engine',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'eye',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'face',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'farm',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'feather',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'finger',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'fish',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'flag',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'floor',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'fly',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'foot',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'fork',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'fowl',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'frame',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'garden',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'girl',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'glove',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'goat',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'gun',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'hair',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'hammer',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'hand',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'hat',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'head',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'heart',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'hook',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'horn',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'horse',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'hospital',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'house',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'island',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'jewel',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'kettle',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'key',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'knee',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'knife',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'knot',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'leaf',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'leg',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'library',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'line',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'lip',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'lock',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'map',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'match',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'monkey',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'moon',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'mouth',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'muscle',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'nail',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'neck',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'needle',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'nerve',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'net',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'nose',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'nut',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'office',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'orange',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'oven',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'parcel',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'pen',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'pencil',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'picture',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'pig',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'pin',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'pipe',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'plane',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'plate',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'plough',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'pocket',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'pot',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'potato',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'prison',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'pump',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'rail',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'rat',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'receipt',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'ring',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'rod',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'roof',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'root',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'sail',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'school',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'scissors',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'screw',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'seed',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'sheep',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'shelf',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'ship',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'shirt',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'shoe',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'skin',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'skirt',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'snake',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'sock',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'spade',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'sponge',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'spoon',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'spring',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'square',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'stamp',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'star',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'station',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'stem',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'stick',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'stocking',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'stomach',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'store',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'street',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'sun',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'table',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'tail',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'thread',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'throat',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'thumb',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'ticket',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'toe',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'tongue',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'tooth',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'town',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'train',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'tray',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'tree',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'trousers',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'umbrella',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'wall',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'watch',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'wheel',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'whip',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'whistle',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'window',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'wing',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'wire',
            'type' => 'noun'
        ]);
        Word::create([
            'name' => 'worm ',
            'type' => 'noun'
        ]);
    }
}
