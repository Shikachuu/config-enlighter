<?php

namespace Database\Seeders;

use App\Models\Word;
use Illuminate\Database\Seeder;

class AdjectiveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Word::create([
            'name' => 'different',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'used',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'important',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'every',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'large',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'available',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'popular',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'able',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'basic',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'known',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'various',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'difficult',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'several',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'united',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'historical',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'hot',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'useful',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'mental',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'scared',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'additional',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'emotional',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'old',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'political',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'similar',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'healthy',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'financial',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'medical',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'traditional',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'federal',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'entire',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'strong',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'actual',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'significant',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'successful',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'electrical',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'expensive',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'pregnant',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'intelligent',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'interesting',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'poor',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'happy',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'responsible',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'cute',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'helpful',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'recent',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'willing',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'nice',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'wonderful',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'impossible',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'serious',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'huge',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'rare',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'technical',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'typical',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'competitive',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'critical',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'electronic',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'immediate',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'aware',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'educational',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'environmental',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'global',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'legal',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'relevant',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'accurate',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'capable',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'dangerous',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'dramatic',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'efficient',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'powerful',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'foreign',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'hungry',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'practical',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'psychological',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'severe',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'suitable',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'numerous',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'sufficient',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'unusual',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'consistent',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'cultural',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'existing',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'famous',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'pure',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'afraid',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'obvious',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'careful',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'latter',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'unhappy',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'acceptable',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'aggressive',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'boring',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'distinct',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'eastern',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'logical',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'reasonable',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'strict',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'administrative',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'automatic',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'civil',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'former',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'massive',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'southern',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'unfair',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'visible',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'alive',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'angry',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'desperate',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'exciting',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'friendly',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'lucky',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'realistic',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'sorry',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'ugly',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'unlikely',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'anxious',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'comprehensive',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'curious',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'impressive',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'informal',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'inner',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'pleasant',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'sexual',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'sudden',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'terrible',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'unable',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'weak',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'wooden',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'asleep',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'confident',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'conscious',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'decent',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'embarrassed',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'guilty',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'lonely',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'mad',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'nervous',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'odd',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'remarkable',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'substantial',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'suspicious',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'tall',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'tiny',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'more',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'some',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'one',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'all',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'many',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'most',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'other',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'such',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'even',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'new',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'just',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'good',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'any',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'each',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'much',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'own',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'great',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'another',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'same',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'few',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'free',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'right',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'still',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'best',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'public',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'human',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'both',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'local',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'sure',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'better',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'general',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'specific',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'enough',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'long',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'small',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'less',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'high',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'certain',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'little',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'common',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'next',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'simple',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'hard',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'past',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'big',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'possible',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'particular',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'real',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'major',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'personal',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'current',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'left',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'national',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'least',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'natural',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'physical',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'short',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'last',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'single',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'individual',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'main',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'potential',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'professional',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'international',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'lower',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'open',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'according',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'alternative',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'special',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'working',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'true',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'whole',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'clear',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'dry',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'easy',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'cold',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'commercial',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'full',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'low',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'primary',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'worth',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'necessary',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'positive',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'present',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'close',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'creative',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'green',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'late',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'fit',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'glad',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'proper',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'complex',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'content',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'due',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'effective',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'middle',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'regular',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'fast',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'independent',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'original',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'wide',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'beautiful',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'complete',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'active',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'negative',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'safe',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'visual',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'wrong',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'ago',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'quick',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'ready',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'straight',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'white',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'direct',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'excellent',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'extra',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'junior',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'pretty',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'unique',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'classic',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'final',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'overall',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'private',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'separate',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'western',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'alone',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'familiar',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'official',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'perfect',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'bright',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'broad',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'comfortable',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'flat',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'rich',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'warm',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'young',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'heavy',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'valuable',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'correct',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'leading',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'slow',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'clean',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'fresh',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'normal',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'secret',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'tough',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'brown',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'cheap',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'deep',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'objective',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'secure',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'thin',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'chemical',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'cool',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'extreme',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'exact',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'fair',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'fine',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'formal',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'opposite',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'remote',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'total',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'vast',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'lost',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'smooth',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'dark',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'double',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'equal',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'firm',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'frequent',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'internal',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'sensitive',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'constant',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'minor',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'previous',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'raw',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'soft',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'solid',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'weird',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'amazing',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'annual',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'busy',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'dead',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'false',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'round',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'sharp',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'thick',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'wise',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'equivalent',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'initial',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'narrow',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'nearby',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'proud',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'spiritual',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'wild',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'adult',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'apart',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'brief',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'crazy',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'prior',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'rough',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'sad',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'sick',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'strange',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'external',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'illegal',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'loud',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'mobile',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'nasty',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'ordinary',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'royal',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'senior',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'super',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'tight',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'upper',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'yellow',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'dependent',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'funny',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'gross',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'ill',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'spare',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'sweet',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'upstairs',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'usual',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'brave',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'calm',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'dirty',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'downtown',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'grand',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'honest',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'loose',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'male',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'quiet',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'brilliant',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'dear',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'drunk',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'empty',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'female',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'inevitable',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'neat',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'ok',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'representative',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'silly',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'slight',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'smart',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'stupid',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'temporary',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'weekly',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'that',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'this',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'what',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'which',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'time',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'these',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'work',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'no',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'only',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'then',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'first',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'money',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'over',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'business',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'his',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'game',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'think',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'after',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'life',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'day',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'home',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'economy',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'away',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'either',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'fat',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'key',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'training',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'top',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'level',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'far',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'fun',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'house',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'kind',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'future',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'action',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'live',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'period',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'subject',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'mean',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'stock',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'chance',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'beginning',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'upset',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'chicken',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'head',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'material',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'salt',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'car',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'appropriate',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'inside',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'outside',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'standard',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'medium',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'choice',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'north',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'square',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'born',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'capital',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'shot',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'front',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'living',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'plastic',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'express',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'feeling',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'otherwise',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'plus',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'savings',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'animal',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'budget',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'minute',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'character',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'maximum',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'novel',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'plenty',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'select',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'background',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'forward',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'glass',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'joint',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'master',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'red',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'vegetable',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'ideal',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'kitchen',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'mother',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'party',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'relative',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'signal',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'street',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'connect',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'minimum',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'sea',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'south',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'status',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'daughter',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'hour',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'trick',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'afternoon',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'gold',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'mission',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'agent',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'corner',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'east',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'neither',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'parking',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'routine',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'swimming',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'winter',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'airline',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'designer',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'dress',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'emergency',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'evening',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'extension',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'holiday',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'horror',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'mountain',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'patient',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'proof',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'west',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'wine',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'expert',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'native',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'opening',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'silver',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'waste',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'plane',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'leather',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'purple',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'specialist',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'bitter',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'incident',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'motor',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'pretend',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'prize',
            'type' => 'adjective'
        ]);
        Word::create([
            'name' => 'resident',
            'type' => 'adjective'
        ]);
    }
}
