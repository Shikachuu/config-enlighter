<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Configuration;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ConfigurationFactory extends Factory
{
    protected $model = Configuration::class;

    public function definition(): array
    {
        return [
            'language' => $this->faker->mimeType,
            'include_gc' => $this->faker->boolean(60),
            'edit_token' => Str::random()
        ];
    }
}
