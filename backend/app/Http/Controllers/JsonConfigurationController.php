<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Configuration;
use App\Models\Revision;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class JsonConfigurationController extends Controller
{
    public function index(): JsonResponse
    {
        return response()->json(Configuration::query()->paginate(10));
    }

    public function show(string $configId): JsonResponse
    {
        $config = Configuration::query()->find($configId);

        if (is_null($config)) {
            return response()->json([
                'id' => $configId,
                'success' => false,
                'error' => 'Config not found'
            ], 404);
        }

        return response()->json($config);
    }

    public function store(Request $request): JsonResponse
    {
        $requestBody = $request->json()->all();

        $validator = \Validator::make($requestBody, [
            'language' => ['string', 'min:1', 'max:255'],
            'include_gc' => ['boolean'],
            'content' => ['string', 'min:3', 'required']
        ]);

        if ($validator->fails()) {
            return response()->json(
                [
                    'success' => false,
                    'error' => 'Unable to create configuration, wrong body content',
                    'request_body' => $requestBody
                ],
                400
            );
        }

        $configuration = Configuration::create([]);

        $revision = new Revision();
        $revision->content = $requestBody['content'];
        $configuration->revision()->save($revision);

        Log::info($configuration->id . " configuration created.");

        return response()->json(['id' => $configuration->id, 'edit_token' => $configuration->edit_token], 201);
    }

    public function update(Request $request, string $configId): JsonResponse
    {
        $requestBody = $request->json()->all();

        $validator = \Validator::make($requestBody, [
            'content' => ['string', 'min:3', 'required']
        ]);

        if ($validator->fails()) {
            return response()->json(
                [
                    'success' => false,
                    'error' => 'Unable to create configuration, wrong body content',
                    'request_body' => $requestBody
                ],
                400
            );
        }

        $config = Configuration::query()->find($configId);

        if (is_null($config)) {
            return response()->json([
                'id' => $configId,
                'success' => false,
                'error' => 'Config not found'
            ], 404);
        }

        $revision = new Revision();
        $revision->content = $requestBody['content'];
        $config->revision()->save($revision);

        Log::info("$configId configuration updated");

        return response()->json(['id' => $config->id, 'edit_token' => $config->edit_token], 201);
    }
}
