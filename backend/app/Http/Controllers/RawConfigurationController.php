<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Configuration;
use Illuminate\Http\Response;

class RawConfigurationController extends Controller
{
    public function show(string $configId): Response
    {
        $content = Configuration::query()->find($configId);

        if (is_null($content)) {
            return response('404 - Not found', 404, ['Content-Type' => 'text/plain']);
        }

        return response(
            $content->content,
            200,
            ['Content-Type' => 'text/plain']
        );
    }
}
