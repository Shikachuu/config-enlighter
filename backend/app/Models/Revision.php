<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Revision extends Model
{
    protected $fillable = [
        'revision_id',
        'content'
    ];

    protected $hidden = ['id'];

    public $timestamps = true;

    protected static function boot()
    {
        parent::boot();
        static::creating(function (Revision $revision) {
            /** @var Configuration $configuraiton */
            $configuraiton = $revision->configuration()->first();

            $lastId = $configuraiton->revision()
                    ->orderBy('revision_id', 'desc')
                    ->first()
                    ->revision_id ?? 0;

            $revision->revision_id = $lastId + 1;
        });
    }

    public function configuration()
    {
        return $this->belongsTo(Configuration::class);
    }
}
