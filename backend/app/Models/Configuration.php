<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Configuration extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public $timestamps = true;

    protected $fillable = [
        'language',
        'include_gc',
    ];
    protected $guarded = ['edit_token'];
    protected $hidden = ['edit_token'];

    protected $appends = ['content'];

    protected $casts = ['include_gc' => 'boolean'];

    protected static function generateId(): string
    {
        $adjectives = Word::query()->where('type', '=', 'adjective')->get();
        $nouns = Word::query()->where('type', '!=', 'adjective')->get();

        $id = ucfirst($adjectives->get(rand(0, $adjectives->count() - 1))->name);
        $id .= ucfirst($adjectives->get(rand(0, $adjectives->count() - 1))->name);
        $id .= ucfirst($nouns->get(rand(0, $nouns->count() - 1))->name);

        if (Configuration::query()->where('id', '=', $id)->count() > 0) {
            $id = self::generateId();
        }

        return $id;
    }

    protected static function boot()
    {
        parent::boot();
        static::creating(function (Configuration $configuration) {
            $configuration->id = self::generateId();
            $configuration->edit_token = Str::random();
        });
    }

    public function revision()
    {
        return $this->hasMany(Revision::class, 'configuration_id', 'id');
    }

    public function getContentAttribute()
    {
        return $this->revision()
                ->orderBy('revision_id', 'desc')
                ->first()->content ?? '';
    }

    public function addRevision(string $content): void
    {
        $revision = new Revision();
        $revision->content = $content;

        $this->revision()->save($revision);
    }
}
