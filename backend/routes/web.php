<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('health', function () {
    return response()->json([
        'ok' => true,
        'db' => is_null(\Illuminate\Support\Facades\DB::getName()) === false
    ]);
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('config', 'JsonConfigurationController@index');
    $router->post('config', 'JsonConfigurationController@store');
    $router->get('config/{configId:[A-Z][a-z]+[A-Z][a-z]+[A-Z][a-z]+}', 'JsonConfigurationController@show');
    $router->put('config/{configId:[A-Z][a-z]+[A-Z][a-z]+[A-Z][a-z]+}', 'JsonConfigurationController@update');
    $router->get('raw/{configId:[A-Z][a-z]+[A-Z][a-z]+[A-Z][a-z]+}', 'RawConfigurationController@show');
});
