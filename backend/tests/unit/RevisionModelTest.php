<?php

declare(strict_types=1);

use Faker\Factory;

class RevisionModelTest extends TestCase
{
    public function testRevisionIdGeneration()
    {
        /** @var \App\Models\Configuration $configuration */
        $configuration = \App\Models\Configuration::factory(1)->create()->first();

        $configuration->addRevision(Factory::create()->realTextBetween(50));

        $this->assertEquals(1, $configuration->revision()->first()->revision_id);

        $configuration->addRevision(Factory::create()->realTextBetween(50));
        $configuration->addRevision(Factory::create()->realTextBetween(50));
        $configuration->addRevision(Factory::create()->realTextBetween(50));

        $this->assertEquals(4, $configuration->revision()->count());

        $this->assertEquals(
            4,
            $configuration->revision()->orderBy('revision_id', 'desc')->first()->revision_id
        );
        $this->assertEquals(
            $configuration->revision()->orderBy('revision_id', 'desc')->first()->content,
            $configuration->content
        );
    }
}
