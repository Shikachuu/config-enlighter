<?php

declare(strict_types=1);

class ConfigurationModelTest extends TestCase
{
    public function testIdGenerator()
    {
        $configuration = \App\Models\Configuration::factory(1)->create();
        $this->assertTrue(
            (bool)preg_match('/^([A-Z][a-z]+[A-Z][a-z]+[A-Z][a-z]+)$/', $configuration->first()->id)
        );
    }
}
