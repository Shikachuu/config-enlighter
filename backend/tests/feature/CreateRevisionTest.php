<?php

declare(strict_types=1);

use App\Models\Configuration;
use Faker\Factory;

class CreateRevisionTest extends TestCase
{
    public function testUpdate()
    {
        /** @var \App\Models\Configuration $configuration */
        $configuration = \App\Models\Configuration::factory(1)->create()->first();
        $configuration->addRevision(Factory::create()->realTextBetween(50));

        $content = [
            'content' => 'fun heightChecker(heights: IntArray) = (heights zip heights.sorted()).fold(0) { count, pair -> if (pair.first == pair.second) count else count + 1 }',
        ];

        $response = $this->call(
            method: 'PUT',
            uri: '/api/config/' . $configuration->id,
            content: json_encode($content, JSON_UNESCAPED_UNICODE)
        );

        /** @var array<string, mixed> $responseBody */
        $responseBody = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('id', $responseBody);
        $this->assertArrayHasKey('edit_token', $responseBody);
        $this->assertEquals(201, $response->status());

        $this->seeInDatabase('configurations', ['id' => $responseBody['id']]);
        $this->seeInDatabase(
            'revisions',
            [
                'revision_id' => 2,
                'configuration_id' => $configuration->id
            ]
        );

        $this->get('/api/config/' . $configuration->id)
            ->seeJsonContains($content)
            ->assertResponseOk();
    }

    public function testNonExsitingUpdate()
    {
        $content = [
            'content' => 'fun heightChecker(heights: IntArray) = (heights zip heights.sorted()).fold(0) { count, pair -> if (pair.first == pair.second) count else count + 1 }',
        ];

        $response = $this->call(
            method: 'PUT',
            uri: '/api/config/TesztElekEgyet',
            content: json_encode($content, JSON_UNESCAPED_UNICODE)
        );

        $this->assertEquals(404, $response->status());

        /** @var array<string, mixed> $responseBody */
        $responseBody = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('success', $responseBody);
        $this->assertArrayHasKey('error', $responseBody);

        $this->assertEquals(false, $responseBody['success']);
        $this->assertEquals('Config not found', $responseBody['error']);
    }

    public function testBadRequestBody()
    {
        /** @var \App\Models\Configuration $configuration */
        $configuration = \App\Models\Configuration::factory(1)->create()->first();
        $configuration->addRevision(Factory::create()->realTextBetween(50));

        $this->put('/api/config/' . $configuration->id)
            ->seeJsonContains([
                'success' => false,
                'error' => 'Unable to create configuration, wrong body content'
            ])
            ->assertResponseStatus(400);
    }
}
