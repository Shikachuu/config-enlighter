<?php

declare(strict_types=1);

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ListAllConfigTest extends TestCase
{
    public function testFirstPage()
    {
        $this->get('/api/config')
            ->seeJsonContains(\App\Models\Configuration::query()->first()->toArray())
            ->seeJsonContains(['current_page' => 1])
            ->assertResponseOk();
    }

    public function testSecondPage()
    {
        $this->get('/api/config?page=2')
            ->seeJsonContains(\App\Models\Configuration::query()->offset(11)->first()->toArray())
            ->assertResponseOk() ;
    }
}
