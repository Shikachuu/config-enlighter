<?php

declare(strict_types=1);

class GetRawConfigTest extends TestCase
{
    public function testGetAnExistingConfigRaw()
    {
        /** @var \App\Models\Configuration $configuration */
        $configuration = \App\Models\Configuration::factory(1)->create()->first();

        $this->get('/api/raw/' . $configuration->id)
            ->seeHeader('Content-Type', 'text/plain; charset=UTF-8')
            ->assertResponseOk();

        $this->assertEquals($configuration->content, $this->response->getContent());
    }

    public function testNotfoundExitingConfigRaw()
    {
        $this->get('/api/raw/AsdAsdAsd')
            ->seeHeader('Content-Type', 'text/plain; charset=UTF-8')
            ->assertResponseStatus(404);
        $this->response->assertSeeText('404 - Not found');
    }
}
