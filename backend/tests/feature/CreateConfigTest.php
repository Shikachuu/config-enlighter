<?php

declare(strict_types=1);

class CreateConfigTest extends TestCase
{
    public function testCreateWithContentOnly()
    {
        $response = $this->call(
            method: 'POST',
            uri: '/api/config',
            content: json_encode(['content' => 'Teszt'])
        );
        /** @var array<string, mixed> $responseBody */
        $responseBody = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('id', $responseBody);
        $this->assertArrayHasKey('edit_token', $responseBody);

        $this->seeInDatabase('configurations', ['id' => $responseBody['id']]);
    }

    public function testCreateWithEveryField()
    {
        $response = $this->call(
            method: 'POST',
            uri: '/api/config',
            content: json_encode([
                'content' => 'fun heightChecker(heights: IntArray) = (heights zip heights.sorted()).fold(0) { count, pair -> if (pair.first == pair.second) count else count + 1 }',
                'include_gc' => false,
                'language' => 'kotlin',
            ])
        );
        /** @var array<string, mixed> $responseBody */
        $responseBody = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('id', $responseBody);
        $this->assertArrayHasKey('edit_token', $responseBody);

        $this->seeInDatabase('configurations', ['id' => $responseBody['id']]);
    }

    public function testBadRequestBody()
    {
        $this->post('/api/config/')
            ->seeJsonContains([
                'success' => false,
                'error' => 'Unable to create configuration, wrong body content'
            ])
            ->assertResponseStatus(400);
    }
}
