<?php

declare(strict_types=1);

class GetOneConfigTest extends TestCase
{
    public function testGetAnExistingConfigById()
    {
        /** @var \App\Models\Configuration $configuration */
        $configuration = \App\Models\Configuration::factory(1)->create()->first();

        $this->get("/api/config/" . $configuration->id)
            ->seeJsonContains(['language' => $configuration->language])
            ->assertResponseOk();
    }

    public function testGetNonExistingConfig()
    {
        $this->get('/api/config/TesztElekEgyet')
            ->seeJsonContains(['success' => false, 'error' => 'Config not found'])
            ->assertResponseStatus(404);
    }

}
